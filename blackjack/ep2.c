/****************************************************************
    Nome: Erick Rodrigues de Santana
    NUSP: 11222008

    Fonte e comentários: 
        - MAC0110 - Prof. Carlos E. Ferreira - EP2

****************************************************************/

#include <stdio.h>

#define EPS 0.00000001
#define C1 832.8159
#define C2 0.71828182846

int simulaJogada(int, double *);
int geraCarta(double);
double modulo(double);
double seno(double);
double frac(double);

int main() {
    int limiar, simulacao, numeroSimulacoes, pontosJogador, pontosBanca, vitoriasJogador, asterisco;
    double semente, porcentagemVitorias;

    printf("Digite a semente (0 < x < 1): ");
    scanf("%lf", &semente);
    printf("Digite o número de simulações para cada limiar: ");
    scanf("%d", &numeroSimulacoes);

    for (limiar = 12; limiar <= 20; limiar++) {
        vitoriasJogador = 0;

        for (simulacao = 1; simulacao <= numeroSimulacoes; simulacao++) {
            pontosJogador = simulaJogada(limiar, &semente);
            
            if (pontosJogador < 21) {
                pontosBanca = simulaJogada(pontosJogador, &semente);
                if (pontosBanca > 21)
                    vitoriasJogador++;
            }
            else {
                if (pontosJogador == 21)
                    vitoriasJogador++;
            }
        }

        porcentagemVitorias = (double) vitoriasJogador * 100/numeroSimulacoes;
        printf("%d (%.1lf%%): ", limiar, porcentagemVitorias);

        for (asterisco = 1; asterisco <= porcentagemVitorias; asterisco++)
            printf("*");
        
        printf("\n");
    }
    
    return 0;
}

int simulaJogada(int limite, double * semente) {
    int pontuacao = 0;
    double x = *semente;
    while (pontuacao < limite) {
        pontuacao += geraCarta(x);
        x = frac(C1 * modulo(seno(x)) + C2);
    }
    *semente = x;
    return pontuacao;
}

int geraCarta(double x) {
    int carta;
    carta = 13 * x + 1;
    if (carta > 10)
        carta = 10;
    return carta;
}

double modulo(double x) {
    if (x < 0)
        return -x;
    return x;
}

double seno(double x) {
    double termo = x, seno = 0.0;
    int k = 1;
    while (modulo(termo) > EPS) {
        seno = seno + termo;
        k = k + 1;
        termo = -termo * x * x / ((2*k - 1) * (2*k - 2));
    }
    return seno;
}

double frac(double numero) {
    return numero - (int) numero;
}