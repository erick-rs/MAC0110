/****************************************************************
    Nome: Erick Rodrigues de Santana
    NUSP: 11222008

    Fonte e comentários: 
        - MAC0110 - Prof. Carlos E. Ferreira - EP4
        - Como o professor vai copiar algumas funções minhas, deixei explícito o que ele deve copiar
        com comentarios "RECORTE DAQUI..... ATÈ AQUI" e com meu NUSP em algumas funções

****************************************************************/

#include <stdio.h>

/************************************** (1) RECORTE DAQUI **************************************/
#define TAM 8

const int vetx[8] = {-1, -1, -1, 0, 1, 1, 1, 0};
const int vety[8] = {-1, 0, 1, 1, 1, 0, -1, -1};

/* Essa função auxilia as funções de verificação de jogada com alguns casos especiais
    (por exemplo se passou dos limites do tabuleiro e etc) */
int verificacoesEspeciais11222008(int i, int j, int k) {
    if ((i + vetx[k] != -1) && (i + vetx[k] != TAM) && (j + vety[k] != -1) && (j + vety[k] != TAM))
        return 1;
        
    return 0;
}

/* Essa função contém o processo de verificação de uma jogada (se ela é válida ou não)
    Como muitas funções utilizam esse mesmo algoritmo, optei por colocá-lo em uma função separada */
int verificaJogada11222008(int tabuleiro[TAM][TAM], int cor, int l, int c, int k) {
    int i = vetx[k] + l;
    int j = vety[k] + c;

    if (tabuleiro[i][j] == cor || i < 0 || i >= TAM || j < 0 || j >= TAM)
        return -1;

    while (tabuleiro[i][j] == -cor && verificacoesEspeciais11222008(i, j, k)) {
        i += vetx[k];
        j += vety[k];
    }

    if (tabuleiro[i][j] == cor)
        return 1;

    return 0;
}
/************************************** (1) ATÉ AQUI **************************************/

int podejogar(int tabuleiro[TAM][TAM], int cor, int l, int c) {
    int k, ehJogadaValida;

    if (tabuleiro[l][c] != 0) /* caso o jogador jogue numa posição não vazia */ 
        return 0;

    for (k = 0, ehJogadaValida = 0; k < 8 && ehJogadaValida == 0; k++) {
        ehJogadaValida = verificaJogada11222008(tabuleiro, cor, l, c, k);
        if (ehJogadaValida == -1) {
            ehJogadaValida = 0;
            continue;
        }
    }
    
    return ehJogadaValida;
}

void joga(int tabuleiro[TAM][TAM], int cor, int l, int c) {
    int i, j, k, jogada;

    tabuleiro[l][c] = cor;

    for (k = 0; k < 8; k++) {
        jogada = verificaJogada11222008(tabuleiro, cor, l, c, k);
        if (jogada == -1)
            continue;

        if (jogada == 1) {
            i = vetx[k] + l;
            j = vety[k] + c;

            while (tabuleiro[i][j] == -cor && verificacoesEspeciais11222008(i, j, k) == 1) {
                tabuleiro[i][j] = cor;
                i += vetx[k];
                j += vety[k];
            }
        }
    }
}

/************************************** (2) RECORTE DAQUI **************************************/
void copiaTabuleiro11222008(int tabuleiro[TAM][TAM], int copTabuleiro[TAM][TAM]) {
    int i, j;

    for (i = 0; i < TAM; i++)
        for (j = 0; j < TAM; j++)
            copTabuleiro[i][j] = tabuleiro[i][j];
}

/* Retorna a quantidade máxima de pontos que o adversário poderá fazer na próxima rodada */
int simulaProximaJogada11222008(int tabuleiro[TAM][TAM], int cor, int l, int c) {
    int copTabuleiro[TAM][TAM], i, j, k, pecasObtidas, maiorQtdePecas;

    copiaTabuleiro11222008(tabuleiro, copTabuleiro);
    joga(copTabuleiro, cor, l, c);

    maiorQtdePecas = 0;
    for (l = 0; l < TAM; l++) {
        for (c = 0; c < TAM; c++) {
            if (podejogar(copTabuleiro, -cor, l, c)) {
                pecasObtidas = 0;
                for (k = 0; k < 8; k++) {
                    if (verificaJogada11222008(copTabuleiro, -cor, l, c, k)) {
                        i = vetx[k] + l;
                        j = vety[k] + c;

                        while (copTabuleiro[i][j] == cor && verificacoesEspeciais11222008(i, j, k) == 1) {
                            pecasObtidas = pecasObtidas + 1;
                            i += vetx[k];
                            j += vety[k];
                        }
                    }
                }

                if (pecasObtidas >= maiorQtdePecas)
                    maiorQtdePecas = pecasObtidas;
            }
        }
    }

    return maiorQtdePecas;
}

/* O objetivo do bot é jogar em lugares onde a diferença entre a quantidade de pecas que ele conseguirá e
    a quantidade de peças o usuário conseguirá na próxima jogada for máxima
    OBS: Como os cantos do tabuleiro são posições importantes, caso o bot consiga jogar em uma dessas posições,
    ele irá jogar independente da quantidade de pontos que ele conseguir */
void escolheJogada11222008(int tabuleiro[TAM][TAM], int cor, int *linha, int *coluna) {
    int i, j, k, cantos, pecasObtidas, pontosAdversario, maiorDiferenca, maiorQtdePecas, existeJogadaValida;
    int li, co, linhaPerfeita, linhaPerfeita2, colunaPerfeita, colunaPerfeita2;

    maiorDiferenca = maiorQtdePecas = existeJogadaValida = 0;
    for (li = 0, cantos = 0; li < TAM && cantos == 0; li++) {
        for (co = 0; co < TAM && cantos == 0; co++) {
            if (podejogar(tabuleiro, cor, li, co)) {
                existeJogadaValida = 1;
                pecasObtidas = 0;
                if ((li == 0 && co == 0) || 
                    (li == 0 && co == TAM-1) || 
                    (li == TAM-1 && co == 0) || 
                    (li == TAM-1 && co == TAM-1)) {
                    linhaPerfeita = li;
                    colunaPerfeita = co;
                    cantos = 1;
                }
                else {
                    for (k = 0; k < 8; k++) {
                        if (verificaJogada11222008(tabuleiro, cor, li, co, k)) {
                            i = vetx[k] + li;
                            j = vety[k] + co;

                            while (tabuleiro[i][j] == -cor && verificacoesEspeciais11222008(i, j, k) == 1) {
                                pecasObtidas = pecasObtidas + 1;
                                i += vetx[k];
                                j += vety[k];
                            }
                        }
                    }

                    pontosAdversario = simulaProximaJogada11222008(tabuleiro, cor, li, co);

                    if ((pecasObtidas - pontosAdversario) > maiorDiferenca) {
                        maiorDiferenca = pecasObtidas - pontosAdversario;
                        linhaPerfeita = li;
                        colunaPerfeita = co;
                    }
                    
                    if (pecasObtidas > maiorQtdePecas) {
                        maiorQtdePecas = pecasObtidas;
                        linhaPerfeita2 = li;
                        colunaPerfeita2 = co;
                    }
                }
            }
        }
    }

    if (maiorDiferenca == 0 && cantos == 0) {
        linhaPerfeita = linhaPerfeita2;
        colunaPerfeita = colunaPerfeita2;
    }

    if (existeJogadaValida == 1) {
        *linha = linhaPerfeita;
        *coluna = colunaPerfeita;
    }
}
/************************************** (2) ATÉ AQUI **************************************/

/* Conta quantas peças há do jogador com certa cor */
int calculaPontos(int tabuleiro[TAM][TAM], int cor) {
    int i, j, pontos;

    for (i = 0, pontos = 0; i < TAM; i++)
        for (j = 0; j < TAM; j++)
            if (tabuleiro[i][j] == cor)
                pontos = pontos + 1;
    
    return pontos;
}

/* Vê onde o usuário pode jogar */
void calculaPossibilidades(int tabuleiro[TAM][TAM], int cor) {
    int i, j;

    for (i = 0; i < TAM; i++)
        for (j = 0; j < TAM; j++)
            if (podejogar(tabuleiro, cor, i, j))
                tabuleiro[i][j] = 7;
}

/* Zera todas as possibilidades de jogada da rodada anterior */
void zeraPossibilidades(int tabuleiro[TAM][TAM]) {
    int i, j;

    for (i = 0; i < TAM; i++)
        for (j = 0; j < TAM; j++)
            if (tabuleiro[i][j] == 7)
                tabuleiro[i][j] = 0;
}

/* A cada rodada, essa função vê se existem jogadas válidas para um jogador
    (Se não existirem jogadas válidas para nenhum dos dois, o jogo acaba) */
int aindaExisteJogadasValidas(int tabuleiro[TAM][TAM], int cor) {
    int i, j, existeJogadasValidas;

    for (i = 0, existeJogadasValidas = 0; i < TAM; i++)
        for (j = 0; j < TAM; j++)
            if (podejogar(tabuleiro, cor, i, j))
                existeJogadasValidas = 1;
    
    return existeJogadasValidas;
}

void imprimeTabuleiro(int tabuleiro[TAM][TAM]) {
    int i, j;

    printf("\n");
    for (j = 0; j < TAM; j++)
        printf("       %d", j);
    
    printf("\n   +-------+-------+-------+-------+-------+-------+-------+-------+\n");
    for (i = 0; i < TAM; i++) {
        printf("%d  |", i);
        for (j = 0; j < TAM; j++) {
            if (tabuleiro[i][j] == 1)
                printf("   w   ");
            else if (tabuleiro[i][j] == -1)
                printf("   P   ");
            else if (tabuleiro[i][j] == 7)
                printf("   +   ");
            else
                printf("       ");
            
            printf("|");
        }
        printf("\n   +-------+-------+-------+-------+-------+-------+-------+-------+\n");
    }
    printf("\n");
}

int main() {
    int i, j, cor, linha, coluna, qtdePretas, qtdeBrancas, casasUsadas, ehUsuario, jogadorRoubou, jogadorPodeJogar, computadorPodeJogar;
    int tabuleiro[TAM][TAM];

    for (i = 0; i < TAM; i++)
        for (j = 0; j < TAM; j++)
            tabuleiro[i][j] = 0;
        
    tabuleiro[(TAM/2)-1][(TAM/2)-1] = tabuleiro[TAM/2][TAM/2] = 1;
    tabuleiro[(TAM/2)-1][TAM/2] = tabuleiro[TAM/2][(TAM/2)-1] = -1;
    casasUsadas = 4;

    printf("********* BEM VINDO AO OTHELO *********\n\n");
    imprimeTabuleiro(tabuleiro);
    printf("\nEscolha as suas peças (-1 para pretas (P) e 1 para brancas (W)): ");
    scanf("%d", &cor);

    ehUsuario = 0;
    if (cor == -1)
        ehUsuario = 1;

    jogadorRoubou = 0; jogadorPodeJogar = computadorPodeJogar = 1;
    while ((computadorPodeJogar || jogadorPodeJogar) && !jogadorRoubou && casasUsadas < 64) {
        if (ehUsuario == 1) {
            ehUsuario = 0;
            if (jogadorPodeJogar) {
                printf("Digite a linha: ");
                scanf("%d", &linha);
                printf("Digite a coluna: ");
                scanf("%d", &coluna);

                if (podejogar(tabuleiro, cor, linha, coluna)) {
                    joga(tabuleiro, cor, linha, coluna);
                    imprimeTabuleiro(tabuleiro);
                    casasUsadas = casasUsadas + 1;
                }
                else {
                    jogadorRoubou = 1;
                    printf("\nVOCÊ TENTOU ROUBAR, LOGO EU VENCI! HAHAHA!\n");
                }
            }   
            else {
                printf("\nVOCÊ PASSOU!! VOU JOGAR NOVAMENTE!\n");
            }
        }
        else {
            ehUsuario = 1;
            computadorPodeJogar = aindaExisteJogadasValidas(tabuleiro, -cor);
            if (computadorPodeJogar) {
                ehUsuario = 1;
                escolheJogada11222008(tabuleiro, -cor, &linha, &coluna);
                joga(tabuleiro, -cor, linha, coluna);
                zeraPossibilidades(tabuleiro);
                printf("\nTABULEIRO APÓS A MINHA JOGADA\n");
                casasUsadas = casasUsadas + 1;
            }
            else {
                printf("\nEU PASSEI!! JOGUE NOVAMENTE!\n");
            }
            calculaPossibilidades(tabuleiro, cor);
            imprimeTabuleiro(tabuleiro);
            zeraPossibilidades(tabuleiro);
            jogadorPodeJogar = aindaExisteJogadasValidas(tabuleiro, cor);
        }
    }

    if (!jogadorPodeJogar && !computadorPodeJogar)
        printf("\nNÃO HÁ MAIS JOGADAS VÁLIDAS!\n");

    if (!jogadorRoubou) {
        if (cor == -1) {
            qtdePretas = calculaPontos(tabuleiro, cor);
            qtdeBrancas = calculaPontos(tabuleiro, -cor);
        }
        else {
            qtdeBrancas = calculaPontos(tabuleiro, cor);
            qtdePretas = calculaPontos(tabuleiro, -cor);
        }
        printf("\nFINAL: BRANCOS %d X %d PRETOS\n", qtdeBrancas, qtdePretas);
        if (qtdeBrancas == qtdePretas)
            printf("\nEMPATOU!\n");
        else {
            if (qtdeBrancas > qtdePretas)
                printf("\nO BRANCO GANHOU!\n");
            else
                printf("\nO PRETO GANHOU!\n");
        }
    }

    return 0;
}