/****************************************************************
    Nome: Erick Rodrigues de Santana
    NUSP: 11222008

    Fonte e comentários: 
        - MAC0110 - Prof. Carlos E. Ferreira - EP3

****************************************************************/

#include <stdio.h>
#include <math.h>

#define LI 1000
#define CO 4
#define HORAS 24

int buscaVisitante(int, int[LI][CO], int); // retorna a linha da matriz na qual o visitante está ou -1 se não o encontrar
void incluiVisitante(int, int, int, int[LI][CO], int *);
void incrementaHistograma(int, int, int[HORAS]);
void calculaTempoPermanencia(int, int, int, int[LI][CO]);
int contaUsuarios(int[LI][CO], int); // conta quantos usuários são diferentes do total de visitantes
double calculaMediaPermanencia(int[LI][CO], int);
double calculaDesvioPadrao(int[LI][CO], double, int);
void trocaLinha(int[CO], int[CO]); // troca as linhas da matriz (vetores) de posição
void ordenaNUSP(int[LI][CO], int);
void ordenaTempoPermanencia(int[LI][CO], int);
void distribuicaoTempoPermanencia(int[LI][CO], int);
void exibeVisitantes(int[LI][CO], int);
void exibeHistograma(int[HORAS]);

/* UMA BREVE EXPLICAÇÃO SOBRE A MATRIZ VISITANTES:
   - A primeira coluna refere-se ao NUSP do visitante;
   - A segunda coluna refere-se a hora em que o visitante entrou no salão;
   - A terceira coluna refere-se ao minuto em que o visitante entrou;
   - A última coluna refere-se ao tempo de permanência em minutos;
   Ex: quando aparecer no código visitantes[i][3], estou acessando o tempo de permanência do visitante da posição i;
*/
 
int main() {
    int visitantes[LI][CO], histograma[HORAS], entraSai, nusp, horas, minutos, posicaoVisitante, visitas, auxVisitas, usuarios;
    double mediaPermanencia, desvioPadrao;
    char nomeArquivo[80];
    FILE * arquivo;
    
    printf("Bem-vindo ao Sistema Estatítico de Uso do Salão de Estudos da Biblioteca Carlos Benjamim de Lyra - IME-USP\n\n");
    printf("Digite o nome do arquivo de dados: ");
    scanf("%s", nomeArquivo);
    arquivo = fopen(nomeArquivo, "r");

    visitas = 0;
    while (!feof(arquivo)) {
        if (fscanf(arquivo, "%d %d %d:%d", &entraSai, &nusp, &horas, &minutos) != 4)
            continue;

        if (entraSai == 0) {
            incluiVisitante(nusp, horas, minutos, visitantes, &visitas);
            visitas++;
        } 
        else {
            posicaoVisitante = buscaVisitante(nusp, visitantes, visitas);
            if (posicaoVisitante == -1) { // entrou antes do relatório
                incluiVisitante(nusp, 0, 0, visitantes, &visitas);
                incrementaHistograma(visitantes[visitas][1], horas, histograma);
                calculaTempoPermanencia(visitas, horas, minutos, visitantes);
                visitas++;
            } 
            else {
                incrementaHistograma(visitantes[posicaoVisitante][1], horas, histograma);
                calculaTempoPermanencia(posicaoVisitante, horas, minutos, visitantes); 
            }
        }
    }

    // calcula o tempo de permanência das pessoas que estão na biblioteca no momento do relatório (00:00)
    for (auxVisitas = visitas-1; auxVisitas >= 0; auxVisitas--) {
        if (visitantes[auxVisitas][3] == 0) {
            incrementaHistograma(visitantes[auxVisitas][1], 23, histograma);
            calculaTempoPermanencia(auxVisitas, 23, 59, visitantes);
        }
    }

    usuarios = contaUsuarios(visitantes, visitas);
    mediaPermanencia = calculaMediaPermanencia(visitantes, visitas);
    desvioPadrao = calculaDesvioPadrao(visitantes, mediaPermanencia, visitas);

    printf("\n\nNO PERÍODO TIVEMOS %d USUÁRIOS DIFERENTES E %d VISITAS\n", usuarios, visitas);
    printf("A MÉDIA DE PERMANÊNCIA NO SALÃO FOI DE %.2lf MINUTOS, COM DESVIO PADRÃO DE %.2lf\n\n", mediaPermanencia, desvioPadrao);

    printf("LISTA DE USUÁRIOS DO SALÃO DE LEITURA\n\n");
    ordenaNUSP(visitantes, visitas);
    exibeVisitantes(visitantes, visitas);

    printf("\n\nVISITAS ORDENADAS PELO TEMPO DE PERMANÊNCIA\n\n");
    ordenaTempoPermanencia(visitantes, visitas);
    exibeVisitantes(visitantes, visitas);

    printf("\n\nHISTOGRAMA POR HORA\n\n");
    exibeHistograma(histograma);

    printf("\n\nDISTRIBUIÇÃO DE USUÁRIOS POR TEMPO DE USO (EM MINUTOS):\n\n");
    distribuicaoTempoPermanencia(visitantes, visitas);

    return 0;
}

/* busca do último item para o primeiro. A busca foi implementada dessa maneira pois,
se um usuário estiver mais de uma vez na matriz, o primeiro nusp que será encontrado será o da sua última visita,
visita esta que possui a última hora que ele entrou. Caso a busca fosse do primeiro para o último, essas situações
deveriam ser verificadas, o que aumentaria o código e complicaria o seu entendimento. */
int buscaVisitante(int nusp, int visitantes[LI][CO], int visitas) {
    int i, achei;
    for (i = visitas-1, achei = -1; i >= 0 && achei == -1; i--)
        if (nusp == visitantes[i][0])
            achei = i;

    return achei;
}

// inclui um visitante novo na primeira linha vazia da matriz
void incluiVisitante(int nusp, int hora, int minuto, int visitantes[LI][CO], int * visitas) {
    visitantes[*visitas][0] = nusp;
    visitantes[*visitas][1] = hora;
    visitantes[*visitas][2] = minuto;
    visitantes[*visitas][3] = 0;
}

void incrementaHistograma(int horaEntrada, int horaSaida, int histograma[HORAS]) {
    while (horaEntrada <= horaSaida) {
        histograma[horaEntrada]++;
        horaEntrada++;
    }
}

void calculaTempoPermanencia(int pos, int horaSaida, int minutoSaida, int visitantes[LI][CO]) {
    visitantes[pos][3] = (60*horaSaida + minutoSaida) - (60*visitantes[pos][1] + visitantes[pos][2]);
}

int contaUsuarios(int visitantes[LI][CO], int visitas) {
    int i, j, usuarios, achei;
    for (i = 0, usuarios = visitas; i < visitas-1; i++) {
        achei = 0;
        for (j = i+1; j < visitas && achei == 0; j++) {
            if (visitantes[i][0] == visitantes[j][0]) {
                achei = 1;
                usuarios = usuarios - 1;
            }
        }
    }

    return usuarios;
}

double calculaMediaPermanencia(int visitantes[LI][CO], int visitas) {
    int i, soma;
    for (i = 0, soma = 0; i < visitas; i++)
        soma += visitantes[i][3];

    return (double) soma/visitas;
}

double calculaDesvioPadrao(int visitantes[LI][CO], double media, int visitas) {
    double soma;
    int i;
    for (i = 0, soma = 0.0; i < visitas; i++)
        soma += (double) pow((visitantes[i][3] - media), 2);

    return sqrt(soma/visitas);
}

void trocaLinha(int v1[CO], int v2[CO]) {
    int i, aux;
    for (i = 0; i < CO; i++) {
        aux = v1[i];
        v1[i] = v2[i];
        v2[i] = aux;
    }
}

void ordenaNUSP(int visitantes[LI][CO], int visitas) { // bubble sort
    int i, j;
    for (i = 0; i < visitas; i++)
        for (j = 0; j < visitas-i-1; j++)
            if (visitantes[j][0] > visitantes[j+1][0])
                trocaLinha(visitantes[j], visitantes[j+1]);
}

void ordenaTempoPermanencia(int visitantes[LI][CO], int visitas) { // bubble sort
    int i, j;
    for (i = 0; i < visitas; i++)
        for (j = 0; j < visitas-i-1; j++)
            if (visitantes[j][3] < visitantes[j+1][3])
                trocaLinha(visitantes[j], visitantes[j+1]);
}

void distribuicaoTempoPermanencia(int visitantes[LI][CO], int visitas) {
    int i, j, usuarios, soma;
    for (i = 100, soma = 0; i <= 1000; i = i+100) {
        for (j = 0, usuarios = 0; j < visitas; j++)
            if (visitantes[j][3] > i-100 && visitantes[j][3] < i)
                usuarios++;
        soma += usuarios;
        printf("De %d minutos até %d minutos: %d usuário(s)\n", (i-100), (i-1), usuarios);
    }
    printf("%d minutos ou mais: %d usuário(s)\n", (i-100), (visitas-soma));
} 

void exibeVisitantes(int visitantes[LI][CO], int visitas) {
    int i;
    printf("   NUSP\t\tHora de Entrada\t\tTempo de Permanência (minutos)\n");
    for (i = 0; i < visitas; i++)
        printf("%8.d           %02d:%02d                       %4.d\n", visitantes[i][0], visitantes[i][1], visitantes[i][2], visitantes[i][3]);
}

void exibeHistograma(int histograma[HORAS]) {
    int i, j;
    for (i = 0; i < HORAS; i++) {
        printf("%02d: |", i);
        for (j = 0; j < histograma[i]; j++)
            printf("*");
        printf("\n");
    }
}