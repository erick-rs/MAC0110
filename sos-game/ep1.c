/****************************************************************
    Nome: Erick Rodrigues de Santana
    NUSP: 11222008

    Fonte e comentários: 
        - MAC0110 - Prof. Carlos E. Ferreira - EP1
        - Como o professor disse que a jogada do computador poderia ser a mais simples possível, optei por
        fazer um bot que joga a letra oposta a que o usuário jogou, na primeira casa livre que ele encontrar
        do tabuleiro

****************************************************************/
#include <stdio.h>

int findBoardPosition(int, int);
int isValidPlay(int, int, int); 
int computerPlay(int, int);
int userPlay(int, int, int);
int calculateExpo(int, int);
int updateScore(int, int);
int showBoard(int);

int main() {
    int usedBoardPositions = 0, boardConfiguration = 0, userScore = 0, computerScore = 0, lastScoringRead = 0, winner = 0;
    int currentPlayer, move, row, column;
    printf("*** Bem-vindo ao Jogo do SOS! ***\n");
    printf("\nDigite 1 se voce deseja comecar, ou 2 caso contrario: ");
    scanf("%d", &currentPlayer);
    printf("Configuração Inicial:\n\n");
    showBoard(boardConfiguration);
    printf("\n\nO tabuleiro tem %d SOS(s)\n", (userScore+computerScore));
    printf("Placar: Usuário %d X %d Computador\n", userScore, computerScore);

    while (winner == 0) {
        if (currentPlayer == 1) {
            printf("\nDigite sua jogada:\n");
            printf("Digite 1 para S, 2 para 0: ");
            scanf("%d", &move);
            printf("Digite a linha: ");
            scanf("%d", &row);
            printf("Digite a coluna: ");
            scanf("%d", &column);
            if (move < 1 || move > 2) {
                printf("Jogada inválida!");
            }
            else if ((row < 0 || row > 3) || (column < 0 || column > 3)) {
                printf("Linhas ou colunas inválidas!");
            }
            else if (isValidPlay(row, column, boardConfiguration) == 0) {
                printf("Você jogou numa posição não vazia!");
            }
            else {
                printf("Tabuleiro após a sua jogada:\n");
                boardConfiguration += userPlay(move, row, column);
                lastScoringRead = userScore;
                userScore += updateScore(boardConfiguration, (userScore+computerScore));
                if (userScore-lastScoringRead > 0) {
                    if (usedBoardPositions == 8)
                        printf("Você marcou %d ponto(s).\n", (userScore-lastScoringRead));
                    else
                        printf("Você marcou %d ponto(s). Jogue novamente.\n", (userScore-lastScoringRead));
                }
                else {
                    currentPlayer = 2;
                }

                showBoard(boardConfiguration);
                usedBoardPositions++;
            }
        }
        else {
            printf("\nTabuleiro após a minha jogada:\n");
            boardConfiguration += computerPlay(move, boardConfiguration);
            lastScoringRead = computerScore;
            computerScore += updateScore(boardConfiguration, (userScore+computerScore));
            if (computerScore-lastScoringRead > 0) {
                if (usedBoardPositions == 8)
                    printf("Marquei %d ponto(s).\n", (computerScore-lastScoringRead));
                else
                    printf("Marquei %d ponto(s). Vou jogar novamente.\n", (computerScore-lastScoringRead));
            }
            else {
                currentPlayer = 1;
            }

            showBoard(boardConfiguration);
            usedBoardPositions++;
        }   

        printf("\n\nO tabuleiro tem %d SOS(s)\n", (userScore+computerScore));
        printf("Placar: Usuário %d X %d Computador\n", userScore, computerScore);

        if (usedBoardPositions == 9) {
            winner = 1;
            if (userScore > computerScore)
                printf("\nVocê ganhou!");
            else if (userScore < computerScore)
                printf("\nEu ganhei!");
            else
                printf("\nEmpatamos!");
        }
    }

    printf("\n");
    return 0;
}

int findBoardPosition(int row, int column) {
    int i = 1, j = 1, k;
    for (k = 0; k < 9; k++) {
        if (row == i && column == j)
            return k;

        if (j == 3) {
            i++;
            j = 1;
        }
        else 
            j++;
    }

    return 0;
}

int isValidPlay(int row, int column, int board) {
    int k = findBoardPosition(row, column);
    int exp = calculateExpo(k, 1);
    if ((board/exp)%3 == 0)
        return 1;
    return 0;
}

int computerPlay(int move, int board) {
    int isFirstEmpty = 0, k, newBoard;

    for (k = 0; k < 9 && isFirstEmpty == 0; k++) {
        if (board%3 == 0) {
            isFirstEmpty = 1;
            // se o jogador jogou S, o computador joga O (2*3^k); caso contrário, 1*3^k
            if (move == 1) 
                newBoard = calculateExpo(k, 2);
            else
                newBoard = calculateExpo(k, 1);
        }
        board /= 3;
    }

    return newBoard;
}

int userPlay(int move, int row, int column) {
    int k = findBoardPosition(row, column);
    int sum;
    if (move == 1) // se o jogador jogou S, 1*3^k; se jogou O, 2*3^k
        sum = calculateExpo(k, 1);
    else 
        sum = calculateExpo(k, 2);

    return sum;
}

int calculateExpo(int k, int initialValue) {
    int sum = initialValue; // pode começar com 1 ou 2
    while (k > 0) {
        sum *= 3;
        k--;
    }

    return sum;
}

/* para pegar qualquer digito de um número na base 3, podemos utilizar 
a fórmula (n/3^(k-1))%3, onde n é o número na base decimal e k é o digito que queremos pegar, da direita pra esquerda.
Ex: para pegar o 4 digito do número 3855 ==> (3855/3^(4-1))%3 = 1 
*/
int updateScore(int board, int currentScore) {
    int totalScore = 0;
    int expo0 = calculateExpo(0, 1); // 3^0
    int expo1 = calculateExpo(1, 1); // 3^1
    int expo2 = calculateExpo(2, 1); // ..
    int expo3 = calculateExpo(3, 1); // ..
    int expo4 = calculateExpo(4, 1);
    int expo5 = calculateExpo(5, 1);
    int expo6 = calculateExpo(6, 1);
    int expo7 = calculateExpo(7, 1);
    int expo8 = calculateExpo(8, 1); // 3^8
    // pontuação nas linhas
    if ((board/expo0)%3 == 1 && (board/expo1)%3 == 2 && (board/expo2)%3 == 1)
        totalScore++;
    if ((board/expo3)%3 == 1 && (board/expo4)%3 == 2 && (board/expo5)%3 == 1)
        totalScore++;
    if ((board/expo6)%3 == 1 && (board/expo7)%3 == 2 && (board/expo8)%3 == 1)
        totalScore++;
    // pontuação nas colunas
    if ((board/expo0)%3 == 1 && (board/expo3)%3 == 2 && (board/expo6)%3 == 1)
        totalScore++;
    if ((board/expo1)%3 == 1 && (board/expo4)%3 == 2 && (board/expo7)%3 == 1)
        totalScore++;
    if ((board/expo2)%3 == 1 && (board/expo5)%3 == 2 && (board/expo8)%3 == 1)
        totalScore++;
    // pontuação nas diagonais
    if ((board/expo0)%3 == 1 && (board/expo4)%3 == 2 && (board/expo8)%3 == 1)
        totalScore++;
    if ((board/expo2)%3 == 1 && (board/expo4)%3 == 2 && (board/expo6)%3 == 1)
        totalScore++;

    return (totalScore-currentScore);
}

int showBoard(int board) {
    int i, j;
    for (i = 1; i <= 3; i++) {
        for (j = 1; j <= 3; j++) {
            if (board%3 == 0)
                printf("     ");
            else if (board%3 == 1)
                printf("  S  ");
            else
                printf("  O  ");
            
            if (j < 3)
                printf("|");

            board /= 3;
        }
        if (i < 3)
            printf("\n-----+-----+-----\n");
    }

    return 0;
}